package com.wordpress.haelnote.springbootrestredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootrestredisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootrestredisApplication.class, args);
	}

}
